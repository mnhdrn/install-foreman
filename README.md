Role Name
=========

this role will activate any needed repository to get the foreman-katello-installer binary.
and will run the binary with some parameters

Requirements
------------

Centos 7, 8 or Red Hat 7

Role Variables
--------------

- **organization**: default name for the organization
- **location**: default name for the location
- **admin_username**: name of the admin user that will be created during installation
- **admin_password**: password for the admin user

Dependencies
------------
none

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - role: install-foreman
           vars:
             organization: startx
             location: paris
             admin_username: administrator
             admin_password: password

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
